# SCV Infosec Report Generator

SCV Infosec Report Generator produces quarterly infosec reports for SCV employees and
uploads it to GitHub / GitLab.

## CI

[TBD] Build for linux and macOS (runner / cloud?)

## Packages

* Cobra: subcommand pattern;
* [TBD] TUI?
* HTTP to POST gists to GitHub / GitLab
  * can we get away with using the user's private key?
  * otherwise we may need a private access token
