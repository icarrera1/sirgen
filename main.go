/*
Copyright © 2023 SCVSoft

*/
package main

import "gitlab.com/icarrera1/sirgen/cmd"

func main() {
	cmd.Execute()
}
